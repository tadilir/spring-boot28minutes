package com.in28minutes.springboot.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

// /login => Helloworld
@Controller
public class LoginController {

	@RequestMapping("/login")
	public String loginMessage(@RequestParam String name) {
		return "login";
	}
	

}
